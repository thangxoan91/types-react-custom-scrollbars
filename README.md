# Installation
> `npm install --save @types/react-custom-scrollbars`

# Summary
This package contains type definitions for react-custom-scrollbars (https://github.com/malte-wessel/react-custom-scrollbars).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-custom-scrollbars

Additional Details
 * Last updated: Sat, 04 Aug 2018 00:56:31 GMT
 * Dependencies: react
 * Global values: none

# Credits
These definitions were written by  David-LeBlanc-git <https://github.com/David-LeBlanc-git>, kittimiyo <https://github.com/kittimiyo>.
